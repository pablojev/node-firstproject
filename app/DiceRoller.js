class DiceRoller {
    constructor() {
        this.numbers = [];
    }

    oneRoll() {
        // Math.random(); // <0, 1)
        // Math.floor(), Math.ceil(), Math.round(x)
        // 1, 6
        return Math.floor( Math.random() * 6 ) + 1;
    }

    roll(num) {
        // Metoda roll, przyjmie argument numeryczny i doda
        // do tablicy znajdujacej sie w polu 'numbers' 
        // wszystkie wylosowane wartosci.
        for(let i = 0; i < num; i++) {
            this.numbers.push( this.oneRoll() );
        }
    }

    sum() {
        // zwraca sume wszystkich elementow
        let sum = 0;
        for(let elem of this.numbers) {
            sum += elem;
        }
        return sum;
    }

    avg() {
        // zwraca srednia arytmetyczna wszystkich elementow
        return this.sum() / this.numbers.length;
    }

    get(arg) {
        return arg(this.numbers);
    }

}

module.exports = DiceRoller;
const DiceRoller = require('./app/DiceRoller');

let dr = new DiceRoller();
dr.roll(5);
console.log(dr.numbers);
console.log( dr.sum(), dr.avg() );

let sumOfDice = dr.get(function(arr) {
    let sum = 0;
    for(let e of arr) 
        sum += e;
    return sum;
});

let minOfRolls = dr.get((arr) => {
    // szukamy minimalmnej wartosci rzutu kostka w naszych
    // rzutach
    let min = arr[0];
    for(let e of arr) {
        if(e < min) {
            min = e;
        }
    }
    console.log('MIN: ' + min);
    return min;
});

console.log(sumOfDice);